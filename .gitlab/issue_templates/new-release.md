<!--
This template is for tracking a release of aeva. Please replace the
following strings with the associated values:

  - `VERSION`
  - `MAJOR`
  - `MINOR`

Please remove this comment.
-->

# Preparatory steps

  - Update aeva guides
    - Assemble release notes into `doc/release/aeva-VERSION`.
      - [ ] Get positive review and merge.

# Update aeva

If making a release from the `release` branch, e.g., `vMAJOR.MINOR.0-RC2 or above`:

  - [ ] Update `release` branch for **aeva**
```
git fetch origin
git checkout release
git merge --ff-only origin/release
```
  - [ ] Update `version.txt` and tag the commit
```
git checkout -b update-to-vVERSION
echo VERSION > version.txt
git commit -m 'Update version number to VERSION' version.txt
git tag -a -m 'aeva VERSION' vVERSION HEAD
```
  - Integrate changes to `master` branch
    - [ ] Create a merge request targeting `master` (do *not* add `Backport: release`)
    - [ ] Get positive review
    - [ ] `Do: merge`
  - Integrate changes to `release` branch
    - [ ] `git push origin update-to-vVERSION:release vVERSION`

  - Update documentation page
    - [ ] See `https://github.com/Kitware/paraview-docs/blob/gh-pages/versions.json`


# Update aeva-superbuild

If making a release from the `release` branch, e.g., `vMAJOR.MINOR.0-RC2 or above`:

  - Update `release` branch for **aeva/aeva-superbuild**
```
git fetch origin
git checkout release
git merge --ff-only origin/release
```
  - Update `CMakeLists.txt`
    - [ ] Set aeva source selections in `CMakeLists.txt` and force explicit
      version in `CMakeLists.txt`:
```
# Force source selection setting here.
set(aeva_SOURCE_SELECTION "VERSION" CACHE STRING "Force version to VERSION" FORCE)
set(aeva_FROM_SOURCE_DIR OFF CACHE BOOL "Force source dir off" FORCE)
```
  - Update `versions.cmake`
    - [ ] Guide selections in `versions.cmake`
    - [ ] `git add versions.cmake CMakeLists.txt`
    - [ ] `git commit -m "Update the default version to VERSION"`
  - Integrate changes to `master` branch
    - [ ] Create a merge request targeting `master`, title beginning with WIP (do *not* add `Backport: release` to description)
    - [ ] Build binaries (`Do: test`)
    - [ ] Download the binaries that have been generated in the dashboard results. They will be deleted within 24 hours.
    - [ ] Remove explicit version forcing added in CMakeLists.txt, amend the commit, and force push
```
git add CMakeLists.txt
git commit --amend
git gitlab-push -f
```
  - Finalize merge request
    - [ ] Remove WIP from merge request title
    - [ ] Get positive review
    - [ ] `Do: merge`
    - [ ] `git tag -a -m 'aeva superbuild VERSION' vVERSION HEAD`
  - Integrate changes to `release` branch
    - [ ] `git push origin update-to-vVERSION:release vVERSION`

# Validating binaries

  - Binary checklist
    - [ ] macOS
    - [ ] Linux
    - [ ] Windows

# Upload binaries

  - Upload binaries
  - [ ] Ask @chuck.atkins to sign macOS binary
  - [ ] Verify the binaries are uploaded

```
buildListing.sh
updateMD5sum.sh vMAJOR.MINOR
```

  - [ ] Test download links on https://www.computationalmodelbuilding.org/download

# Upload documentation

  - [ ] Verify documentation is uploaded and is correct

# Post-release

  - [ ] Write and publish blog post with release notes.
  - [ ] Post an announcement in the Announcements category on
        [discourse.kitware.com](https://discourse.kitware.com/c/aeva/).
  - [ ] Update release notes
    (https://www.paraview.org/Wiki/ParaView_Release_Notes)
  - [ ] Move unclosed issues to next release milestone in GitLab

/cc @ben.boeckel
/cc @bob.obara
/cc @tjcorona
/cc @dcthomp
/label ~"priority:required"
