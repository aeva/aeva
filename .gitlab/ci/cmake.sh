#!/bin/sh

set -e

readonly version="3.22.1"

case "$( uname -s )" in
    Linux)
        shatool="sha256sum"
        sha256sum="73565c72355c6652e9db149249af36bcab44d9d478c5546fd926e69ad6b43640"
        uplatform="Linux"
        lplatform="linux"
        arch="x86_64"
        ;;
    Darwin)
        shatool="shasum -a 256"
        sha256sum="9ba46ce69d524f5bcdf98076a6b01f727604fb31cf9005ec03dea1cf16da9514"
        uplatform="macos"
        lplatform="macos"
        arch="universal"
        ;;
    *)
        echo "Unrecognized platform $( uname -s )"
        exit 1
        ;;
esac
readonly shatool
readonly sha256sum
readonly uplatform
readonly lplatform
readonly arch

readonly ufilename="cmake-$version-$uplatform-$arch"
readonly lfilename="cmake-$version-$lplatform-$arch"
readonly tarball="$ufilename.tar.gz"

cd .gitlab

echo "$sha256sum  $tarball" > cmake.sha256sum
curl -OL "https://github.com/Kitware/CMake/releases/download/v$version/$tarball"
$shatool --check cmake.sha256sum
tar xf "$tarball"
mv "$lfilename" cmake

if [ "$( uname -s )" = "Darwin" ]; then
    ln -s CMake.app/Contents/bin cmake/bin
fi
