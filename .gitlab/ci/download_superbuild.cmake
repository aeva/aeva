cmake_minimum_required(VERSION 3.12)

# Tarballs are uploaded by CI to the "ci/smtk" folder, In order to use a new superbuild, please move
# the item from the date stamped directory into the `keep` directory. This makes them available for
# download without authentication and as an indicator that the file was used at some point in CI
# itself.

set(data_host "https://data.kitware.com")

# Determine the tarball to download.
# 20250105 - Update to use Xcode 16.1
if ("$ENV{CMAKE_CONFIGURATION}" MATCHES "vs2022")
  set(file_id "1Sog5u2zxbgVSdSLo7rSThtnmVo7rSixh")
  set(file_hash "d573c889cb5eba20603f14f4814834c975332aeb740531a5f8d5eaacebacbe197ed792d0c7e64d6c8df35c15ab8ed1446d49486c45a9d1c3566d84e77928958a")
elseif ("$ENV{CMAKE_CONFIGURATION}" MATCHES "macos")
  set(file_id "1Dx1vHRTJ154ffe0cbUiiNGK2qCEvm2lb")
  set(file_hash "cb6944d83bb22e605b44123f13246b28a00e8356bcecee092c341a9a83941884efa6f8e3848ca7f946d1680a95d81830f627659e49bd8ae45fa213d31f8c58e8")
else ()
  message(FATAL_ERROR
    "Unknown build to use for the superbuild")
endif ()

# Ensure we have a hash to verify.
if (NOT DEFINED file_id OR NOT DEFINED file_hash)
  message(FATAL_ERROR
    "Unknown file and hash for the superbuild")
endif ()

# Download the file.
file(DOWNLOAD
  "https://drive.usercontent.google.com/download?export=download&id=${file_id}&export=download&authuser=0&confirm=t"
  ".gitlab/superbuild.tar.gz"
  STATUS download_status
  EXPECTED_HASH "SHA512=${file_hash}")

# Check the download status.
list(GET download_status 0 res)
if (res)
  list(GET download_status 1 err)
  message(FATAL_ERROR
    "Failed to download superbuild.tar.gz: ${err}")
endif ()

# Extract the file.
execute_process(
  COMMAND
    "${CMAKE_COMMAND}"
    -E tar
    xf ".gitlab/superbuild.tar.gz"
  RESULT_VARIABLE res
  ERROR_VARIABLE err
  ERROR_STRIP_TRAILING_WHITESPACE)
if (res)
  message(FATAL_ERROR
    "Failed to extract superbuild.tar.gz: ${err}")
endif ()
