# aeva/CMB 2.0

aeva/CMB is an application for the annotation and exchange of anatomical models
based on
[ParaView](https://paraview.org/),
[SMTK/CMB](https://computationalmodelbuilder.org/), and
[ITK](https://itk.org).
It is currently focused on annotating unstructured data,
as opposed to aeva/Slicer which is focused on structured (image) data.

While this release is version 2.0, we did not publicize our initial release
as it targeted internal users.

## General notes

Please see the [user's guide and tutorials at read-the-docs](https://aeva.readthedocs.io)
for instructions on using aeva.
For our initial release, we targeted [feature selections](https://simtk.org/plugins/moinmoin/aeva-apps/IndividualFeatures)
as a way to mark up [knee anatomy](https://simtk.org/projects/openknee).
Our plans for the future include models of other organs as well as
annotations of different functional areas (e.g., neuron classification and fiber tractography
as opposed to joint kinematics).
