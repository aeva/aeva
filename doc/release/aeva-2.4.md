# aeva/CMB 2.4

aeva/CMB is an application for the annotation and exchange of anatomical models
based on
[ParaView](https://paraview.org/),
[SMTK/CMB](https://computationalmodelbuilder.org/), and
[ITK](https://itk.org).
It is currently focused on annotating unstructured data,
as opposed to aeva/Slicer which is focused on structured (image) data.

## Release notes

This release contains continued improvements to the python tracing of SMTK operations;
now traces record a python dictionary that can be used to configure the operation
parameters.

This release also includes two new inspection tools:

+ a mesh inspector that allows you to examine the interior of volumetric meshes
  by extracting the surface of a *crinkle slice* (the set of elements intersected
  by a slice plane); and
+ an image inspector that allows you to slice image data with an arbitrary plane.

Although not accessible by end users without additional work, this release
includes a programmatic interface to a third new 3-d widget for editing coordinate
frames. If you are writing custom operations (including Python operations) and
accept rotation+translation transforms to/from world coordinates, then you
can have your operation expose the coordinate frame widget to users.
See [this note](https://discourse.vtk.org/t/vtkcoordinateframewidget/7379)
for a description of the widget
and [this example](https://gitlab.kitware.com/cmb/smtk/-/blob/master/data/attribute/widgets/gallery-frame.sbt)
attribute resource for how to relate it to items in an attribute.

Finally, this release uses a new version of ParaView that highlights any widgets
with active keyboard shortcuts in blue; you can click on the empty space inside
these widgets to enable/disable the keyboard shortcuts.
This allows users to choose their target when multiple 3-d widgets
that use the same shortcuts are present.
See [this note](https://discourse.paraview.org/t/new-feature-property-widgets-show-when-keyboard-shortcuts-are-active/8583)
for an illustrated discussion.

## General notes

Please see the [user's guide and tutorials at read-the-docs](https://aeva.readthedocs.io)
for instructions on using aeva.
For our initial release, we targeted [feature selections](https://simtk.org/plugins/moinmoin/aeva-apps/IndividualFeatures)
as a way to mark up [knee anatomy](https://simtk.org/projects/openknee).
Our plans for the future include models of other organs as well as
annotations of different functional areas (e.g., neuron classification and fiber tractography
as opposed to joint kinematics).
