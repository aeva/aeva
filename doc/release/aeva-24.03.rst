.. _release-notes-24.03:

===========================
aevaCMB 24.03 Release Notes
===========================

This release uses a newer cmb submodule that forces
ParaView pipelines to be created for all resources (not
just those with renderable geometry). This change should
fix an issue where some resources could not be closed or
saved from the File menu (only if they were created as a
result of an operation instead of loaded from a file).
