=================
aeva User's Guide
=================

aeva (\ **a**\ nnotation and **e**\ xhange of **v**\ irtual **a**\ natomy) is a software suite designed to work with virtual anatomy in various forms. With aeva, one can navigate anatomical information that may be in the form of images (DICOM, NIfTI), surface meshes (stl, ply, vtk) and as volume meshes (vtk, med, exodus). aeva aims to provide import/export of anatomy in various formats and annotation by selecting regions and defining attributes. Templating of annotation can be achieved with simple schemas, e.g. one designed for the knee joint.

aeva software suite currently consists of

* **aevaSlicer**. aevaSlicer will be familiar to users of Slicer. The interface is customized and new features have been added to accommodate a workflow amenable to generation of surface and volume meshes of anatomy from medical images.
* **aevaCMB**. aevaCMB will be familiar to users of ParaView and Computational Model Builder. The interface is customized and new features have been added to support operations for import and export of anatomical representations and for annotation (template based and freeform, including a powerful set of region selection).

.. toctree::
   :maxdepth: 4

   aeva-slicer.rst

.. toctree::
   :maxdepth: 4

   aeva-cmb.rst
